FROM alpine

RUN apk update
RUN apk add wine wine_gecko libpng freetype gnutls samba-winbind-clients
RUN ln -s /usr/bin/wine64 /usr/bin/wine

RUN mkdir /app
ADD entrypoint.sh /start
WORKDIR /app
ENTRYPOINT ["/bin/sh", "/start"]
