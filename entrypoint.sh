#!/bin/sh
if [ ! -f "BeamMP-Server.exe" ]; then
	wget https://beamng-mp.com/server/BeamMP_Server.zip
	unzip BeamMP_Server.zip
	rm BeamMP_Server.zip
fi

wine BeamMP-Server.exe
